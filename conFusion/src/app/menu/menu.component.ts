import { DISH } from './../share/dishes';
import { Component, OnInit, Inject } from '@angular/core';
import { Dish } from '../share/dish';
import { DishService } from './../services/dish.service';
import { flyInOut, expand } from '../animations/app.animation';



@Component({
  selector: "app-menu",
  templateUrl: "./menu.component.html",
  styleUrls: ["./menu.component.scss"],
  host: {
    "[@flyInOut]": "true",
    style: "display: block;"
  },
  animations: [flyInOut(), expand()]
})
export class MenuComponent implements OnInit {
  dishes: Dish[];
  errMess: string;
  constructor(
    private dishService: DishService,
    @Inject("BaseURL") public BaseURL
  ) {}

  ngOnInit() {
    this.dishService
      .getDishe()
      .subscribe(
        dishes => (this.dishes = dishes),
        errMess => (this.errMess = <any>errMess)
      );
  }
}
