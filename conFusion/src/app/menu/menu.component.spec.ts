import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { Dish } from './../share/dish';
import { DishService } from './../services/dish.service';
import { DISH } from './../share/dishes';
import { baseURL } from './../share/baseURL';
import { Observable, of } from 'rxjs';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MenuComponent } from './menu.component';
import { DebugElement } from '@angular/core';
import {  by, By } from 'protractor';

describe('MenuComponent', () => {
  let component: MenuComponent;
  let fixture: ComponentFixture<MenuComponent>;

  beforeEach(async(() => {
    const dishServiceStub = {
      getDishes: function (): Observable<Dish[]> {
        return of(DISH);
      }
    };
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        FlexLayoutModule,
        HttpClientModule,
        RouterTestingModule.withRoutes([
          { path: 'menu', component: MenuComponent }
        ]),
        MatGridListModule,
        MatProgressSpinnerModule
      ],
      declarations: [MenuComponent],
      providers: [
        { provide: 'dishesService', useValue: dishServiceStub },
        { provide: 'BaseURL', useValue: baseURL }
      ]
    }).compileComponents();
    const dishesService = TestBed.get(DishService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should use dishes in the template', () => {
    fixture.detectChanges();

    let de: DebugElement;
    let el: HTMLElement;
    de = fixture.debugElement.query(By.css('h1'));
    el = de.nativeElement;

    expect(el.textContent).toContain(DISH[0].name.toUpperCase());

  });
});
