import { ProcessHTTPMsgService } from './process-httpmsg.service';
import { Injectable } from '@angular/core';
import { Dish } from './../share/dish';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { baseURL } from './../share/baseURL';

@Injectable({
  providedIn: 'root'
})
export class DishService {
  constructor(public http: HttpClient,
    private processHTTPMsg: ProcessHTTPMsgService) {}
  getDishe(): Observable<Dish[]> {
    return this.http.get<Dish[]>(baseURL + 'dishes').pipe(catchError(this.processHTTPMsg.handlerError));
  }

  getDish(id: number): Observable<Dish> {
    return this.http.get<Dish>(baseURL + 'dishes/' + id).pipe(catchError(this.processHTTPMsg.handlerError));
  }

  getFeaturedDish(): Observable<Dish> {
    return this.http.get<Dish[]>(baseURL + 'dishes?featured=true').
    pipe(map(dishes => dishes[0])).
    pipe(catchError(this.processHTTPMsg.handlerError));
  }

  getDishIds(): Observable<number[] | any> {
    return this.getDishe().pipe(map(dishes => dishes.map(dish => dish.id))).pipe(catchError(error => error));
  }

  putDish(dish: Dish): Observable<Dish> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.put<Dish>(baseURL + 'dishes/' + dish.id, dish, httpOptions).pipe(catchError(this.processHTTPMsg.handlerError));
  }
}
