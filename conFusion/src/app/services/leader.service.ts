import { ProcessHTTPMsgService } from './process-httpmsg.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Leader } from './../share/leader';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { baseURL } from './../share/baseURL';

@Injectable({
  providedIn: 'root'
})
export class LeaderService {
  constructor(public http: HttpClient,
    public processHTTPMsgService: ProcessHTTPMsgService) {}
  getLeaders(): Observable<Leader[]> {
    return this.http.get<Leader[]>(baseURL + 'leaders').pipe(catchError(this.processHTTPMsgService.handlerError));
  }

  getLeader(id: string): Observable<Leader> {
    return this.http.get<Leader>(baseURL + 'leaders/' + id).pipe(catchError(this.processHTTPMsgService.handlerError));
  }

  getFeaturedLeader(): Observable<Leader> {
    return this.http.get<Leader>(baseURL + 'leaders?featured=true').pipe(map(dishes => dishes[0]))
    .pipe(catchError(this.processHTTPMsgService.handlerError));
  }
}
