import { delay, catchError } from 'rxjs/operators';
import { ProcessHTTPMsgService } from './process-httpmsg.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, pipe } from 'rxjs';
import { Feedback } from '../share/feedback';
import { baseURL } from '../share/baseURL';

@Injectable({
  providedIn: "root"
})
export class FeedbackService {
  constructor(
    private http: HttpClient,
    private processHTTPMsgService: ProcessHTTPMsgService
  ) {}

  getFeedbacks(): Observable<Feedback[]> {
    return this.http
      .get<Feedback[]>(baseURL + "feedback")
      .pipe(catchError(this.processHTTPMsgService.handlerError));
  }

  getFeedback(firstname: string): Observable<Feedback> {
    return this.http
      .get<Feedback>(baseURL + "feedback?firstname=" + firstname)
      .pipe(catchError(this.processHTTPMsgService.handlerError));
  }
  submitFeedback(feedback: Feedback): Observable<Feedback> {
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-type": "application/json"
      })
    };
    return this.http
      .post<Feedback>(baseURL + "feedback", feedback, httpOptions)
      .pipe(delay(5000))
      .pipe(catchError(this.processHTTPMsgService.handlerError));
  }
}
