import { DishService } from './../services/dish.service';
import { PromotionService } from './../services/promotion.service';
import { Dish } from './../share/dish';
import { Promotion } from './../share/promotion';
import { Component, OnInit, Inject } from '@angular/core';
import { Leader } from '../share/leader';
import { LeaderService } from '../services/leader.service';
import { flyInOut, expand } from '../animations/app.animation';

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
  host: {
    "[@flyInOut]": "true",
    style: "display: block;"
  },
  animations: [flyInOut(), expand()]
})
export class HomeComponent implements OnInit {
  promotion: Promotion;
  dish: Dish;
  leader: Leader;
  errMess: string;
  constructor(
    private promotionService: PromotionService,
    private leaderService: LeaderService,
    private dishService: DishService,
    @Inject('BaseURL') public BaseURL
  ) {}

  ngOnInit() {
    this.promotionService
      .getFeaturedPromotion()
      .subscribe(promotion => (this.promotion = promotion), errMess => this.errMess = <any>errMess);
    this.dishService
      .getFeaturedDish()
      .subscribe(
        dish => (this.dish = dish),
        //errMess => (this.errMess = <any>errMess)
      );
      
    this.leaderService
      .getFeaturedLeader()
      .subscribe(leader => (this.leader = leader), errMess => this.errMess = errMess);
  }
}
