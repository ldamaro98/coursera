import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Comment } from './../share/comment';
import { Leader } from './../share/leader';
import { Dish } from './../share/dish';
import { Params, ActivatedRoute } from '@angular/router';
import { DishService } from './../services/dish.service';
import { Location } from '@angular/common';
import { LeaderService } from '../services/leader.service';
import { switchMap } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { visibility, expand } from '../animations/app.animation';
@Component({
  selector: "app-dishdetail",
  templateUrl: "./dishdetail.component.html",
  styleUrls: ["./dishdetail.component.scss"],
  animations: [
     visibility(),
    expand()
  ]
})
export class DishdetailComponent implements OnInit {
  dish: Dish;
  leaders: Leader;
  dishIds: string[];
  nex: string;
  prev: string;
  commentForm: FormGroup;
  comment: Comment;
  @ViewChild('ff') commentFormDirective;
  dishCopy: Dish;
  formErrors = {
    'author': '',
    'comment': ''
  };
  errMess: string;
  validationMessages = {
    'comment': {
      'required': 'Comment Name is required.',
      'minlength': 'Comment must be at least 2 characters long.',

    },
    'author': {
      'required': 'Author Name is required.',
      'minlength': 'Author Name must be at least 2 characters long.',
      'maxlength': 'Author Name cannot be more than 25 characters long.'
    }
  };
  visibility = 'shown';
  constructor(
    private location: Location,
    private routes: ActivatedRoute,
    private leaderService: LeaderService,
    private dishService: DishService,
    public formBuilder: FormBuilder,
    @Inject('BaseURL') public BaseURL
  ) {

  }

  ngOnInit() {
    
    this.dishService
      .getDishIds()
      .subscribe(dishIds => (this.dishIds = dishIds));

    this.routes.params.
    pipe(switchMap((params: Params) => { this.visibility = 'hidden'; return this.dishService.getDish(+params['id']); }))
      .subscribe(dish => { this.dish = dish; this.dishCopy = dish; this.setPrevNext(dish.id); this.visibility = 'shown'; },
        errmess => this.errMess = <any>errmess);
    this.leaderService
      .getFeaturedLeader()
      .subscribe(leaders => (this.leaders = leaders));
      //this.createForm();
  }

  setPrevNext(dishId: string) {
    const index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[
      (this.dishIds.length + index - 1) % this.dishIds.length
    ];
    this.nex = this.dishIds[
      (this.dishIds.length + index + 1) % this.dishIds.length
    ];
  }

 /* createForm() {
    this.commentForm = this.formBuilder.group({
      rating: 5,
      comment: ['', [Validators.required, Validators.minLength(2)]],
      author: [
        '',
        [Validators.required,
        Validators.minLength(2),
        Validators.maxLength(25)]
      ],
      date: new Date().toISOString()
    });
    this.commentForm.valueChanges.subscribe(data => this.onValueChanged(data));

    //this.onValueChanged(); // (re)set validation messages now
  }*/

  goback(): void {
    this.location.back();
  }

  /*onValueChanged(data?: any) {
    if (!this.commentForm) {
      return;
    }*/
   /* const form = this.commentForm;
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
  }*/

  /*onSubmit() {
    this.comment = this.commentForm.value;
    console.log(this.commentForm.value);
    this.dish.comments.push(this.comment);
    this.dishService.putDish(this.dishCopy)
      .subscribe(dish => {
        this.dish = dish; this.dishCopy = dish;
      },
        errmess => { this.dish = null; this.dishCopy = null; this.errMess = <any>errmess; });
    this.commentForm.reset({
      rating: 5,
      comment: '',
      author: '',
      date: new Date().toISOString()
    });
    this.commentFormDirective.resetForm();
  }*/


}
