import { Directive, ElementRef, Renderer2, HostListener } from '@angular/core';

@Directive({
  selector: "[appHigthligth]"
})
export class HigthligthDirective {
  constructor(public el: ElementRef, public renderer: Renderer2) {}

  @HostListener("mouseenter") onMouseEnter() {
    this.renderer.addClass(this.el.nativeElement, "highlight");
  }
  @HostListener("mouseleave") onmouseleave() {
    this.renderer.removeClass(this.el.nativeElement, "highlight");
  }

}
